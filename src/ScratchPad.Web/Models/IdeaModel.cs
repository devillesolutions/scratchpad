﻿using ScratchPad.Web.Repositories;

namespace ScratchPad.Web.Models
{
    public class IdeaModel : Entity
    {
        public string Title { get; set; }

        public string Text { get; set; }
    }
}