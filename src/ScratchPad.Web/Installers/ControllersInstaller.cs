﻿using System.Web.Http;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace ScratchPad.Web.Installers
{
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                                      .BasedOn<IController>()
                                      .WithServiceSelf()
                                      .LifestyleTransient());

            container.Register(Classes.FromThisAssembly()
                                      .BasedOn<ApiController>()
                                      .WithServiceSelf().LifestyleTransient());
        }
    }
}