﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ScratchPad.Web.Models;
using ScratchPad.Web.Plumbing;

namespace ScratchPad.Web.Installers
{
    public class MvcInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<RouteCollection>().Instance(RouteTable.Routes));
            container.Register(Component.For<HttpConfiguration>().Instance(GlobalConfiguration.Configuration));
            container.Register(Component.For<GlobalFilterCollection>().Instance(GlobalFilters.Filters));
            container.Register(Component.For<IWindsorContainer>().Instance(container));
            container.Register(Component.For<IHttpControllerActivator>().ImplementedBy<WindsorCompositionRoot>());
            container.Register(Component.For<IHttpControllerSelector>().ImplementedBy<NamespaceHttpControllerSelector>());
            container.Register(Component.For<BundleCollection>().Instance(BundleTable.Bundles));
        }
    }
}