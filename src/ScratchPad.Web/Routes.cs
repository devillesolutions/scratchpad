using System.Web.Routing;
using RestfulRouting;
using ScratchPad.Web.Controllers;

[assembly: WebActivator.PreApplicationStartMethod(typeof(ScratchPad.Web.Routes), "Start")]

namespace ScratchPad.Web
{
    public class Routes : RouteSet
    {
        public override void Map(IMapper map)
        {
            map.DebugRoute("routedebug");

	    map.Root<HomeController>(x => x.Index());
        }

        public static void Start()
        {
            var routes = RouteTable.Routes;
            routes.MapRoutes<Routes>();
        }
    }
}