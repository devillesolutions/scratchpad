﻿using System.Collections.Generic;
using System.Web.Http;
using MongoDB.Bson;
using ScratchPad.Web.Models;
using ScratchPad.Web.Repositories;

namespace ScratchPad.Web.Controllers.v1
{
    public class IdeasController : ApiController
    {
        private readonly IIdeaRepository _ideaRepository;

        public IdeasController(IIdeaRepository ideaRepository)
        {
            _ideaRepository = ideaRepository;
        }

        public IEnumerable<IdeaModel> Get()
        {
            return _ideaRepository;
        }

        public IdeaModel Get(ObjectId id)
        {
            return _ideaRepository.GetById(id);
        }

        public void Post([FromBody] IdeaModel idea)
        {
            _ideaRepository.Save(idea);
        }

        public void Delete([FromBody]IdeaModel idea)
        {
            _ideaRepository.Delete(idea);
        }
    }
}