﻿using System.Web.Mvc;

namespace ScratchPad.Web.Controllers
{
    public class HomeController : Controller
    {
         public ActionResult Index()
         {
             return View();
         }
    }
}