﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.Windsor;
using Castle.Windsor.Installer;
using RestfulRouting;

namespace ScratchPad.Web
{
    public class ScratchPadWebApplication : HttpApplication
    {
        private IWindsorContainer _container;

        protected void Application_Start()
        {
	    _container = new WindsorContainer();
            _container.AddFacility<StartableFacility>();
            _container.AddFacility<TypedFactoryFacility>();

            _container.Install(FromAssembly.InThisApplication());

            AreaRegistration.RegisterAllAreas();

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RestfulRoutingRazorViewEngine());

            RouteTable.Routes.MapRoutes<Routes>();
        }

	protected void Application_End()
	{
	    _container.Dispose();
	}
    }
}