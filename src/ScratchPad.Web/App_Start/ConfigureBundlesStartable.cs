﻿using Castle.Core;
using System.Web.Optimization;

namespace ScratchPad.Web.App_Start
{
    public class ConfigureBundlesStartable : IStartable
    {
        private readonly BundleCollection _bundles;

        public ConfigureBundlesStartable(BundleCollection bundles)
        {
            _bundles = bundles;
        }

        public void Start()
        {
            _bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-2.0.2.js"));

            _bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            _bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js"));

            _bundles.Add(new ScriptBundle("~/bundles/knockout")
			    .Include("~/Scripts/knockout-2.2.1.js")
			    .Include("~/Scripts/knockout.mapping-latest.js"));

            _bundles.Add(new ScriptBundle("~/bundles/Home").Include("~/Scripts/namespace.js").Include("~/Scripts/home/ideas.js")
							   .Include("~/Scripts/home/home.js"));

            _bundles.Add(new StyleBundle("~/Content/css")
                            .Include("~/Content/bootstrap.css",
                                     "~/Content/bootstrap-responsive.css",
                                     "~/Content/font-awesome.css",
                                     "~/Content/site.css"));
        }

        public void Stop()
        {
        }
    }
}