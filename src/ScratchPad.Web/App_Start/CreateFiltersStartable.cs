﻿using System.Web.Mvc;
using Castle.Core;

namespace ScratchPad.Web.App_Start
{
    public class CreateFiltersStartable : IStartable
    {
        private readonly GlobalFilterCollection _filters;

        public CreateFiltersStartable(GlobalFilterCollection filters)
        {
            _filters = filters;
        }

        public void Start()
        {
            _filters.Add(new HandleErrorAttribute());
        }

        public void Stop()
        {
        }
    }
}