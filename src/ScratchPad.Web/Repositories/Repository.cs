﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ScratchPad.Web.Repositories
{
    public abstract class Repository<TEntity, TId> : IQueryable<TEntity> where TEntity : IEntity<TId>
    {
        private readonly IDataAccessProvider<TEntity, TId> _provider;

        protected Repository(IDataAccessProvider<TEntity, TId> provider)
        {
            _provider = provider;
        }

	protected virtual TEntity GetById(TId id)
	{
	    return _provider.GetById<TEntity>(id);
	}

	protected virtual void Save(TEntity entity)
	{
	    _provider.Update(entity);
	}

	protected virtual void Add(TEntity entity)
	{
	    _provider.Add(entity);
	}

	protected virtual void Add(IEnumerable<TEntity> entities)
	{
	    _provider.Add(entities);
	}

	protected virtual void Delete(TEntity entity)
	{
	    _provider.Delete(entity);
	}

	protected virtual void SaveAll()
	{
	    _provider.Update(this.AsEnumerable());
	}

        public IEnumerator<TEntity> GetEnumerator()
        {
            return _provider.All<TEntity>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _provider.All<TEntity>().GetEnumerator();
        }

        public Expression Expression
        {
            get { return _provider.All<TEntity>().Expression; }
        }

        public Type ElementType
        {
            get { return _provider.All<TEntity>().ElementType; }
        }

        public IQueryProvider Provider
        {
            get { return _provider.All<TEntity>().Provider; }
        }
    }
}