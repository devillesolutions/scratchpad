﻿using System;

namespace ScratchPad.Web.Repositories
{
    public interface IEntity<out TId>
    {
        TId Id { get; }

        DateTime Created { get; }

        DateTime LastUpdated { get; set; }
    }
}