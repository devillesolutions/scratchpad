﻿using System.Linq;

namespace ScratchPad.Web.Repositories
{
    public interface IRepository<out T> : IQueryable<T>
    {
    }
}