﻿using System;
using System.Linq;
using MongoDB.Bson;
using ScratchPad.Web.Models;

namespace ScratchPad.Web.Repositories
{
    public interface IIdeaRepository : IRepository<IdeaModel>
    {
        IdeaModel GetById(ObjectId id);
        void Save(IdeaModel idea);
        void Delete(IdeaModel idea);
    }
}