﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ScratchPad.Web.Repositories
{
    public interface IDataAccessProvider<in TEntity, in TId> : IDisposable where TEntity : IEntity<TId>
    {
        T GetById<T>(TId id) where T : TEntity;
        T GetSingle<T>(Expression<Func<T, bool>> criteria) where T : TEntity;
        IQueryable<T> All<T>(Expression<Func<T, bool>> criteria) where T : TEntity;
        IQueryable<T> All<T>() where T : TEntity;
        T Add<T>(T entity) where T : TEntity;
        void Add<T>(IEnumerable<T> entities) where T : TEntity;
        T Update<T>(T entity) where T : TEntity;
        void Update<T>(IEnumerable<T> entities) where T : TEntity;
        void Delete<T>(T entity) where T : TEntity;
        void Delete<T>(Expression<Func<T, bool>> criteria) where T : TEntity;
        void DeleteAll<T>() where T : TEntity;
        long Count<T>() where T : TEntity;
        bool Exists<T>(Expression<Func<T, bool>> criteria) where T : TEntity;
    }
}