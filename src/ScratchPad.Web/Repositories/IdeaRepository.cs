﻿using MongoDB.Bson;
using ScratchPad.Web.Models;

namespace ScratchPad.Web.Repositories
{
    public class IdeaRepository : Repository<IdeaModel, ObjectId>, IIdeaRepository
    {
        public IdeaRepository(IDataAccessProvider<IdeaModel, ObjectId> provider) : base(provider)
        {
        }

        public new IdeaModel GetById(ObjectId id)
        {
            return base.GetById(id);
        }

        public new void Save(IdeaModel idea)
        {
            base.Save(idea);
        }

        public new void Delete(IdeaModel idea)
        {
            base.Delete(idea);
        }
    }
}