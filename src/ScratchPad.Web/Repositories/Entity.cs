﻿using System;
using MongoDB.Bson;

namespace ScratchPad.Web.Repositories
{
    public abstract class Entity : IEntity<ObjectId>
    {
        protected Entity()
        {
            Created = DateTime.UtcNow;
            LastUpdated = DateTime.UtcNow;
        }

        public ObjectId Id { get; set; }
        public DateTime Created { get; private set; }
        public DateTime LastUpdated { get; set; }
    }
}