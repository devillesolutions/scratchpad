﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EvilDev.Commons.Settings;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace ScratchPad.Web.Repositories
{
    public class MongoDataAccessProvider : IDataAccessProvider<Entity, ObjectId>
    {
        private readonly MongoDatabase _database;

        public MongoDataAccessProvider(IConnectionstringSettingsProvider settings, string key)
        {
            var url = new MongoUrl(settings[key]);
            var client = new MongoClient(url);
            var server = client.GetServer();
            _database = server.GetDatabase(url.DatabaseName);
            _database.RequestStart();
        }

        private static string GetCollectionName<T>()
        {
            string collectionName;
            if (typeof(T).BaseType == typeof(object))
            {
                collectionName = typeof (T).Name;
            }
            else
            {
                var entityType = typeof (T);
                while (entityType.BaseType != typeof (object) && entityType != typeof (Entity))
                {
                    entityType = entityType.BaseType;
                }

                collectionName = entityType.Name;
            }

            return collectionName;
        }

        private MongoCollection<T> Collection<T>()
        {
            return _database.GetCollection<T>(GetCollectionName<T>());
        }

        public T GetById<T>(ObjectId id) where T : Entity
        {
            return Collection<T>().FindOneByIdAs<T>(id);
        }

        public T GetSingle<T>(Expression<Func<T, bool>> criteria) where T : Entity
        {
            return Collection<T>().AsQueryable<T>().Where(criteria).FirstOrDefault();
        }

        public IQueryable<T> All<T>(Expression<Func<T, bool>> criteria) where T : Entity
        {
            return Collection<T>().AsQueryable<T>().Where(criteria);
        }

        public IQueryable<T> All<T>() where T : Entity
        {
            return Collection<T>().AsQueryable<T>();
        }

        public T Add<T>(T entity) where T : Entity
        {
            Collection<T>().Insert(entity);

            return entity;
        }

        public void Add<T>(IEnumerable<T> entities) where T : Entity
        {
            Collection<T>().InsertBatch(entities);
        }

        public T Update<T>(T entity) where T : Entity
        {
            Collection<T>().Save<T>(entity);

            return entity;
        }

        public void Update<T>(IEnumerable<T> entities) where T: Entity
        {
            foreach (var entity in entities)
            {
                Update(entity);
            }
        }

        public void Delete<T>(T entity) where T : Entity
        {
            Collection<T>().Remove(Query<T>.EQ(x => x.Id, entity.Id));
        }

        public void Delete<T>(Expression<Func<T, bool>> criteria) where T : Entity
        {
            foreach (var entity in All<T>().Where(criteria))
            {
                Delete(entity);
            }
        }

        public void DeleteAll<T>() where T : Entity
        {
            Collection<T>().RemoveAll();
        }

        public long Count<T>() where T : Entity
        {
            return Collection<T>().Count();
        }

        public bool Exists<T>(Expression<Func<T, bool>> criteria) where T : Entity
        {
            return All<T>().Any(criteria);
        }

        public void Dispose()
        {
            _database.RequestDone();
        }
    }
}