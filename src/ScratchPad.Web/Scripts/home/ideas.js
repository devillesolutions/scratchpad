(function() {
  var Idea, IdeaListViewModel, NewIdeaViewModel,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  namespace({
    ScratchPad: IdeaListViewModel = (function() {
      function IdeaListViewModel() {
        this.NewIdea = __bind(this.NewIdea, this);
        this.LoadIdeas = __bind(this.LoadIdeas, this);        this.Ideas = ko.observableArray();
        this.NewIdeaVM = new NewIdeaViewModel(this.Ideas);
        this.LoadIdeas();
      }

      IdeaListViewModel.prototype.LoadIdeas = function() {
        var _this = this;

        return $.get('/api/ideas', function(ideas) {
          var idea;

          return _this.Ideas((function() {
            var _i, _len, _results;

            _results = [];
            for (_i = 0, _len = ideas.length; _i < _len; _i++) {
              idea = ideas[_i];
              _results.push(new Idea(idea));
            }
            return _results;
          })());
        });
      };

      IdeaListViewModel.prototype.NewIdea = function() {
        return this.NewIdeaVM.active(true);
      };

      return IdeaListViewModel;

    })()
  });

  namespace({
    ScratchPad: NewIdeaViewModel = (function() {
      function NewIdeaViewModel(ideas) {
        this.ideas = ideas;
        this.cancel = __bind(this.cancel, this);
        this.save = __bind(this.save, this);
        this.edit = __bind(this.edit, this);
        this.activate = __bind(this.activate, this);
        this.idea = new Idea({
          Title: '',
          Text: '',
          Created: new Date()
        });
        this.active = ko.observable(false);
        this.active.subscribe(this.activate);
      }

      NewIdeaViewModel.prototype.activate = function(active) {
        return this.idea = new Idea({
          Title: '',
          Text: '',
          Created: new Date()
        });
      };

      NewIdeaViewModel.prototype.edit = function(idea, e) {
        this.idea = idea;
        return this.idea.activate(true);
      };

      NewIdeaViewModel.prototype.save = function() {
        var self;

        this.active(false);
        self = this;
        return $.post('/api/ideas', self.idea, function() {
          self.ideas.push(self.idea);
          return self.idea.activate(false);
        });
      };

      NewIdeaViewModel.prototype.cancel = function() {
        this.active(false);
        return this.idea.activate(false);
      };

      return NewIdeaViewModel;

    })()
  });

  namespace({
    ScratchPad: Idea = (function() {
      function Idea(json, initialState) {
        this.json = json;
        if (initialState == null) {
          initialState = false;
        }
        this.activate = __bind(this.activate, this);
        ko.mapping.fromJS(json, {}, this);
        this.active = ko.observable(initialState);
      }

      Idea.prototype.activate = function(active) {
        return this.active(active);
      };

      return Idea;

    })()
  });

}).call(this);
