$ () -> 
	ko.bindingHandlers.OnEnterKeyPressed = {
		init: (element, valueAccessor, allBindingsAccessor, viewModel) ->
			allBindings = allBindingsAccessor();
		    
			$(element).keypress((event) ->
				keyCode = (if event.which then event.which else event.keyCode);
				if keyCode is 13
					allBindings.OnEnterKeyPressed.call(viewModel);
					return false;
				else
					return true;
			)
	}
 ko.applyBindings(new ScratchPad.IdeaListViewModel(), document.getElementById('ideas'))