namespace ScratchPad:
	class IdeaListViewModel
		constructor: ->
			@Ideas = ko.observableArray()
			@NewIdeaVM = new NewIdeaViewModel(@Ideas)
			@LoadIdeas()

		LoadIdeas: =>
			$.get '/api/ideas', (ideas) => 
				@Ideas(new Idea(idea) for idea in ideas)

		NewIdea: => @NewIdeaVM.active(true)

namespace ScratchPad:
	class NewIdeaViewModel
		constructor: (@ideas) ->
			@idea = new Idea({ Title: '', Text: '', Created: new Date()})
			@active = ko.observable(false)
			@active.subscribe @activate

		activate: (active) =>
			@idea = new Idea({ Title: '', Text: '', Created: new Date()})

		edit: (idea, e) =>
			@idea = idea
			@idea.activate(true)
	
		save: =>
			@active(false)
			self = this;
			$.post '/api/ideas', self.idea, () -> 
				self.ideas.push self.idea
				self.idea.activate(false)
	
		cancel: =>
			@active(false)
			@idea.activate(false)

namespace ScratchPad:
	class Idea
		constructor: (@json, initialState = false) ->
			ko.mapping.fromJS(json, {}, this)
	
			@active = ko.observable(initialState)
	
		activate: (active) =>
			@active(active)