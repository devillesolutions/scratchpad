(function() {
  $(function() {
    ko.bindingHandlers.OnEnterKeyPressed = {
      init: function(element, valueAccessor, allBindingsAccessor, viewModel) {
        var allBindings;

        allBindings = allBindingsAccessor();
        return $(element).keypress(function(event) {
          var keyCode;

          keyCode = (event.which ? event.which : event.keyCode);
          if (keyCode === 13) {
            allBindings.OnEnterKeyPressed.call(viewModel);
            return false;
          } else {
            return true;
          }
        });
      }
    };
    return ko.applyBindings(new ScratchPad.IdeaListViewModel(), document.getElementById('ideas'));
  });

}).call(this);
