/// <reference path="~/Scripts/jquery-2.0.2.min.js" />
/// <reference path="~/Scripts/knockout-2.2.1.js" />
/// <reference path="~/Scripts/knockout.mapping-latest.js" />
/// <reference path="~/Scripts/namespace.js" />
/// <reference path="~/Scripts/home/ideas.js" />
;describe("IdeaListViewModel", function() {
  it("Should load pull ideas from api when created", function() {
    var sut;

    spyOn($, "get");
    sut = new ScratchPad.IdeaListViewModel();
    return expect($.get.mostRecentCall.args[0]).toEqual("/api/ideas");
  });
  it("Should activate NewIdeaViewModel when NewIdea() is called", function() {
    var sut;

    sut = new ScratchPad.IdeaListViewModel();
    spyOn(sut.NewIdeaVM, "active").andCallThrough();
    sut.NewIdea();
    expect(sut.NewIdeaVM.active).toHaveBeenCalled();
    return expect(sut.NewIdeaVM.active.mostRecentCall.args[0]).toEqual(true);
  });
});
