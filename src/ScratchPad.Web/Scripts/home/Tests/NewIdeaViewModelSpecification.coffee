`/// <reference path="~/Scripts/jquery-2.0.2.min.js" />
/// <reference path="~/Scripts/knockout-2.2.1.js" />
/// <reference path="~/Scripts/knockout.mapping-latest.js" />
/// <reference path="~/Scripts/namespace.js" />
/// <reference path="~/Scripts/home/ideas.js" />
`
###
 Uncomment the line below, to force resharper to stay running after tests execute, this allows you to use your browsers
 built-in debugging tools to step through tests
###
# jasmine.getEnv().currentRunner_.finishCallback = ->;
describe "NewIdeaViewModel", ->
	ideas = [
		{Title: "Idea 1", Text: "Idea text" },
		{Title: "Idea 2", Text: "Idea text" },
	]

	ideas = ko.observableArray(ideas);

	it "Should initialize empty idea when created", ->
		sut = new ScratchPad.NewIdeaViewModel(ideas);

		expect(sut.active()).toEqual(false);
		expect(sut.ideas).toEqual(ideas);
		expect(sut.idea.Title()).toEqual('');

	return;