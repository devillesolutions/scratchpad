﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Moq;
using Moq.Language.Flow;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;

namespace ScratchPad.Web.Tests.Controllers
{
    public abstract class AutoFixtureSpecification
    {
        protected AutoFixtureSpecification()
        {
            Fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        protected IFixture Fixture { get; private set; }

        protected T Fake<T>()
        {
            return Fixture.Create<T>();
        }

        protected T Dep<T>() where T : class
        {
            Fixture.Freeze<Mock<T>>();
            return Fixture.Create<T>();
        }

        protected IEnumerable<T> FakeMany<T>()
        {
            return Fixture.CreateMany<T>();
        }

        protected IReturnsResult<T> Stub<T, TResult>(Expression<Func<T, TResult>> func) where T : class where TResult : class
        {
            return Dep<Mock<T>>().Setup(func).Returns(Dep<TResult>());
        }
    }
}