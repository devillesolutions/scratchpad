﻿using System;
using System.Linq;
using MongoDB.Bson;
using Moq;
using Ploeh.AutoFixture;
using ScratchPad.Web.Controllers.v1;
using ScratchPad.Web.Models;
using ScratchPad.Web.Repositories;
using SharpTestsEx;
using Xunit;

namespace ScratchPad.Web.Tests.Controllers
{
    public class IdeasControllerSpecification : AutoFixtureSpecification
    {
        private readonly Mock<IIdeaRepository> _repoMock;

        public IdeasControllerSpecification()
        {
            _repoMock = Fixture.Freeze<Mock<IIdeaRepository>>();
            var ideas = Fixture.CreateMany<IdeaModel>();
            _repoMock.Setup(r => r.GetById(It.IsAny<ObjectId>()))
                     .Returns<ObjectId>(id => ideas.First(idea => idea.Id == id))
                     .Verifiable();
            _repoMock.Setup(r => r.Save(It.IsAny<IdeaModel>())).Verifiable();
            _repoMock.Setup(r => r.Delete(It.IsAny<IdeaModel>())).Verifiable();
        }

        [Fact]
        public void WhenIdeasControllerGetsAllIdeas()
        {
            var sut = Fixture.Create<IdeasController>();

            var results = sut.Get();

            results.Should().Have.Count.GreaterThan(0);
        }

        [Fact]
        public void WhenIdeasControllerGetsById()
        {
            var sut = Fixture.Create<IdeasController>();
            var id = Fixture.Create<IIdeaRepository>().First().Id;
            var result = sut.Get(id);
            result.Should().Not.Be.Null();
            result.Id.Should().Be(id);
        }

        [Fact]
        public void WhenIdeasControllerSavesIdea()
        {
            var sut = Fixture.Create<IdeasController>();
            var idea = Fixture.Create<IdeaModel>();

            sut.Post(idea);
            _repoMock.Verify(x => x.Save(It.Is<IdeaModel>(i => i == idea)));
        }

        [Fact]
        public void WhenIdeasControllerDeletesIdea()
        {
            var sut = Fixture.Create<IdeasController>();
            var idea = Fixture.Create<IdeaModel>();
            sut.Delete(idea);

            _repoMock.Verify(r => r.Delete(It.Is<IdeaModel>(i => i == idea)));
        }
    }
}