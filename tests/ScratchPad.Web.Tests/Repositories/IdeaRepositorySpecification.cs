﻿using MongoDB.Driver;
using Moq;
using Ploeh.AutoFixture;
using ScratchPad.Web.Models;
using ScratchPad.Web.Repositories;
using ScratchPad.Web.Tests.Controllers;
using Xunit;

namespace ScratchPad.Web.Tests.Repositories
{
    public class IdeaRepositorySpecification : AutoFixtureSpecification
    {
        public IdeaRepositorySpecification()
        {
            Stub<MongoClient, MongoServer>(c => c.GetServer());
            Stub<MongoServer, MongoDatabase>(s => s.GetDatabase(It.IsAny<string>()));
            Stub<MongoDatabase, MongoCollection<IdeaModel>>(db => db.GetCollection<IdeaModel>(It.IsAny<string>()));
        }
    }
}